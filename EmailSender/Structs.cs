﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailSender
{
    public class Line
    {
        public string Account_ID = "";
        public string Email = "";
        public string FinanceID = "";
        public string InvoiceNumber = "";

        public Line(string line)
        {
            string[] tmp = line.Split(',');
            Account_ID = tmp[0];
            Email = tmp[1];
            FinanceID = tmp[2];
            InvoiceNumber = tmp[3];
        }
    }
}
