﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Data.Odbc;

namespace EmailSender
{
    class Program
    {
        static void Main(string[] args)
        {
            string ConnStr = File.ReadAllLines("ConnStr.txt")[0];

            OdbcConnection conn = new OdbcConnection(ConnStr);
            conn.Open();

            int count = 0;

            do
            {
                Console.Write("Enter Count Limit > ");
            }
            while (!Int32.TryParse(Console.ReadLine(), out count)); //wait for user input

            List<string> lines = new List<string>();
            List<string> outs = new List<string>();

            DateTime stamp = DateTime.Now;

            File.AppendAllText("sent.txt", "# timestamp: " + stamp.ToLongDateString() + " " + stamp.ToLongTimeString() + Environment.NewLine);

            File.AppendAllText("sent.txt", "# --------------------" + Environment.NewLine);

            // see Structs.cs
            File.ReadAllLines("data.csv").ToList().ForEach(x => 
            {
                lines.Add(x); 
            });

            File.Delete("data.csv"); // annoying if the program crashes

            int i = 0;

            string body = null;

            Console.Write("Initializing Identity Variable... "); // Why? does FAB_Journal not have an auto incremental ID?
            int _id = Convert.ToInt32(new OdbcCommand("SELECT TOP 1 ID FROM FAB_Journal ORDER BY ID DESC", conn).ExecuteScalar());
            Console.WriteLine("Done");

            OdbcCommand cmd = new OdbcCommand("", conn);
            cmd.CommandText = @"
INSERT INTO FAB_Journal
(ID, Account_ID, Journal_Type_ID, Date_Time, amount, Notes, Entry_Date_Time)
VALUES
(?, ?, 25, getdate(), 0, 'Sent Email Renewal Notice', getdate())
";
            

            lines.ForEach(x =>
            {
                i++;
                if (i <= count)
                {
                    Console.Write("Sending Email For {0}... ", x.Split(',')[0]);
                    try
                    {
                        Outlook.Application oApp = new Outlook.Application();

                        Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);
                        
                        if (body == null)
                            body = File.ReadAllText("template.html");

                        oMsg.HTMLBody = body;
                        oMsg.Subject = "Ann Arbor Alarm Registration";
                        // Add a recipient.
                        Outlook.Recipients oRecips = (Outlook.Recipients)oMsg.Recipients;
                        // Change the recipient in the next line if necessary.
                        Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(x.Split(',')[1]);
                        oRecip.Resolve();
                        // Send.
                        oMsg.Send();
                        Console.WriteLine("Done");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Outlook Error");
                        Console.WriteLine(ex.Message);
                    }
                    Console.Write("Adding Note For {0}... ", x.Split(',')[0]);
                    File.AppendAllText("sent.txt", x + Environment.NewLine);
                    cmd.Parameters.Clear();
                    _id++;
                    cmd.Parameters.AddWithValue("ID", _id);
                    cmd.Parameters.AddWithValue("accid", x.Split(',')[0]);
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("Done");
                }
                else
                {
                    outs.Add(x);
                }
            });

            Console.WriteLine("Cleaning Up");

            try
            {
                File.WriteAllText("data.csv", String.Join(Environment.NewLine, outs.ToArray()));
                File.AppendAllText("sent.txt", Environment.NewLine + Environment.NewLine);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            conn.Close();
        }
    }
}
